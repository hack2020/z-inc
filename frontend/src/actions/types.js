export const FETCH_PETS = "FETCH_PETS";
export const FETCH_PET = "FETCH_PET";
export const FETCH_SHELTER = "FETCH_SHELTER";
