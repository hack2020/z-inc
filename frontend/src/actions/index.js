import * as actions from "./types";
import findFriend from "../api/findFriend";
import { FETCH_PET, FETCH_PETS, FETCH_SHELTER } from "./types";

export const fetchPets = (type = "") => async (dispatch) => {
  if (!type) {
    const response = await findFriend.get("/pet?limit=100");
    dispatch({ type: actions.FETCH_PETS, payload: response.data });
    return;
  }

  const response = await findFriend.get("/pet", {
    params: {
      type: type,
    },
  });

  dispatch({ type: FETCH_PETS, payload: response.data });
};

export const fetchPet = (petId) => async (dispatch) => {
  const response = await findFriend.get(`/pets/${petId}`);
  dispatch({ type: FETCH_PET, payload: response.data });
};

export const fetchShelter = (shelterId) => async (dispatch) => {
  const response = await findFriend.get(`/shelters/${shelterId}`);
  dispatch({ type: FETCH_SHELTER, payload: response.data });
};
