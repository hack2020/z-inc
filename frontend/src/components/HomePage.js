import React, { useState } from "react";
import { Button } from "@material-ui/core";
import ListIcon from "@material-ui/icons/List";
import DashboardIcon from "@material-ui/icons/Dashboard";
import { Link } from "react-router-dom";

import PetList from "./PetCardList";
import { makeStyles } from "@material-ui/styles";
import PetRegistry from "./PetRegistry/PetRegistry";

const useStyle = makeStyles({
  buttonsDiv: {
    display: "flex",
    justifyContent: "space-between",
    padding: "15px 0px",
  },
  viewModeBtn: {
    width: 125
  },
  createPetCardBtn: {
  },
});

const HomePage = () => {
  const classes = useStyle();
  const [viewMode, setViewMode] = useState("card");

  const renderPets = () => {
    return viewMode === "card" ? <PetList /> : <PetRegistry />;
  };

  return (
    <div>
      <div className={classes.buttonsDiv}>
        <Button
          className={classes.createPetCardBtn}
          variant={"outlined"}
          color={"primary"}
          disableRipple
          component={Link}
          to={"/create_pet_card"}
        >
          Новая карточка животного
        </Button>

        {viewMode === "card" ? (
          <Button
            className={classes.viewModeBtn}
            color={"default"}
            variant={"outlined"}
            disableRipple
            startIcon={<ListIcon />}
            onClick={() => setViewMode("table")}
          >
            Таблица
          </Button>
        ) : (
          <Button
            className={classes.viewModeBtn}
            size={"large"}
            startIcon={<DashboardIcon />}
            onClick={() => setViewMode("card")}
          />
        )}
      </div>
      <div>{renderPets()}</div>
    </div>
  );
};

export default HomePage;
