import React from "react";
import { Typography } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";

import { PET_CARD_FIELDS } from "../../assets/addCardFields";
import { renderField } from "./renderFormField";

const CaptureForm = () => {
  return (
    <div>
      <Typography variant={"h6"}>Сведения об отлове</Typography>
      <Grid container spacing={2} alignItems={"flex-end"}>
        {PET_CARD_FIELDS.capture.map((field) => (
            renderField(field, 6)
        ))}
      </Grid>
    </div>
  );
};

export default CaptureForm;
