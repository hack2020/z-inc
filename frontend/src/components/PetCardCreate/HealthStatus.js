import React from "react";
import { Typography } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import { PET_CARD_FIELDS } from "../../assets/addCardFields";
import { renderField } from "./renderFormField";

const HealthStatus = () => {
  return (
    <div>
      <Typography variant={"h6"}>Сведения о состоянии здоровья</Typography>
      <Grid container spacing={1}>
        {PET_CARD_FIELDS.health_status.map((field) => renderField(field))}
      </Grid>
    </div>
  );
};

export default HealthStatus;
