import React, { useState } from "react";
import { Typography } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import { Field, Form } from "formik";
import { PET_CARD_FIELDS } from "../../assets/addCardFields";
import { TextField } from "formik-material-ui";

const NewOwnersForm = () => {
  const [value, setValue] = useState("individual");

  const renderFields = () => {
    return PET_CARD_FIELDS.new_owners_info[value].map((field) => (
      <Grid item xs={6}>
        <label>{field.verbose_name}</label>
        <Field
          key={field.name}
          variant="outlined"
          component={TextField}
          name={field.name}
          type={field.type}
          fullWidth
        />
      </Grid>
    ));
  };

  return (
    <div>
      <Typography variant={"h6"}>Сведения о новых владельцах</Typography>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <RadioGroup
            row
            aria-label={"ownerType"}
            name={"ownerType"}
            onChange={(event) => setValue(event.target.value)}
            defaultValue={"individual"}
          >
            <FormControlLabel
              control={<Radio />}
              label={"Физическое лицо"}
              value={"individual"}
            />
            <FormControlLabel
              control={<Radio />}
              label={"Юридическое лицо"}
              value={"legalEntity"}
            />
          </RadioGroup>
        </Grid>

        {renderFields()}
      </Grid>
    </div>
  );
};

export default NewOwnersForm;
