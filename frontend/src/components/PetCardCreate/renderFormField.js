import Grid from "@material-ui/core/Grid";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import React from "react";
import { Field } from "formik";
import { TextField } from "formik-material-ui";

export const renderField = (field, mdWidth = 4, key = null, fieldName=null, multiline=false) => {
  if (field.type === "select_radio") {
    return (
      <Grid item xs={12} key={key ? key : field.name}>
        {field.verbose_name && <label>{field.verbose_name}</label>}
        <RadioGroup
          row
          aria-label={field.name}
          name={field.name}
          defaultValue={field.options[0]}
        >
          {field.options.map((option) => (
            <FormControlLabel
              value={option}
              control={<Radio />}
              label={option}
            />
          ))}
        </RadioGroup>
      </Grid>
    );
  }

  return (
    <Grid item xs={12} md={mdWidth} key={key ? key : field.name}>
      <label>{field.verbose_name}</label>
      <Field
        variant="outlined"
        multiline={multiline}
        component={TextField}
        name={!fieldName ? field.name : fieldName}
        type={field.type}
        fullWidth
      />
    </Grid>
  );
};
