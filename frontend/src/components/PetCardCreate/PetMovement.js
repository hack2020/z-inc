import React from "react";
import {Typography} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import {PET_CARD_FIELDS} from "../../assets/addCardFields";
import {renderField} from "./renderFormField";

const PetMovement = () => {
    return (
        <div>
            <Typography variant={"h6"}>Движение животного</Typography>
            <Grid container spacing={1}>
                {PET_CARD_FIELDS.pet_movements.map((field) =>
                    renderField(field)
                )}
            </Grid>
        </div>
    )
}

export default PetMovement;