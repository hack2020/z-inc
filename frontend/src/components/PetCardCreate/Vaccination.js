import React, { useState } from "react";
import { Fab, Typography } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import { PET_CARD_FIELDS } from "../../assets/addCardFields";
import { renderField } from "./renderFormField";
import AddIcon from "@material-ui/icons/Add";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles({
  addRecordBtn: {
    float: "right",
    margin: "10px 0px 50px 0px",
  },
});

const Vaccination = () => {
  const classes = useStyles();
  const [recordsCount, setRecordsCount] = useState(2);

  const renderRecords = () => {
    let records = [];
    for (let i = 0; i < recordsCount; i++) {
      PET_CARD_FIELDS.vaccination.map((field) =>
        records.push(renderField(field, 4, field.name + i, field.name + i))
      );
    }
    return records;
  };

  const onAddClick = () => {
    setRecordsCount(recordsCount + 1);
  };

  return (
    <div>
      <Typography variant={"h6"}>Сведения о вакцинации</Typography>
      <Grid container spacing={1}>
        {renderRecords()}
      </Grid>
      <Fab className={classes.addRecordBtn} size={"small"} onClick={onAddClick}>
        <AddIcon />
      </Fab>
    </div>
  );
};

export default Vaccination;
