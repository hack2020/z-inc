import React from "react";
import { Typography } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";

import { PET_CARD_FIELDS } from "../../assets/addCardFields";
import { renderField } from "./renderFormField";

const BasicInfoForm = () => {
  return (
    <React.Fragment>
      <Grid container spacing={1} alignItems={"flex-end"} key={"generalForm"}>
        {PET_CARD_FIELDS.general.map((field) => renderField(field))}
      </Grid>

      <div>
        <Typography variant={"h6"}>Основные сведения</Typography>
        <Grid container spacing={1} alignItems={"flex-end"} key={"basicForm"}>
          {PET_CARD_FIELDS.main.map((field) => renderField(field))}
        </Grid>
      </div>
    </React.Fragment>
  );
};

export default BasicInfoForm;
