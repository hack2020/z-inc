import React, { useState } from "react";
import { Formik, Form } from "formik";
import { Tabs, Typography } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/styles";
import Tab from "@material-ui/core/Tab";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";

import NewOwnersForm from "./NewOwnersForm";
import BasicInfoForm from "./BasicInfoForm";
import CaptureForm from "./CaptureForm";
import PetMovement from "./PetMovement";
import ParasitesTreatment from "./ParasitesTreatment";
import Vaccination from "./Vaccination";
import HealthStatus from "./HealthStatus";

const useStyle = makeStyles((theme) => ({
  root: {
    // flexGrow: 1,
    backgroundColor: "#fff",
    minHeight: "60rem",
  },
  title: {
    textAlign: "center",
    padding: 10,
  },
  tabs: {
    borderRight: `1px solid rgba(0, 0, 0, 0.12)`,
    width: "90%",
  },
  createButton: {
    width: "10rem",
    padding: 10,
    float: "right",
  },
}));

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

const initialValues = { city: "Москва", date: new Date().getDate() };

const CreatePetCard = () => {
  const classes = useStyle();
  const [currentTab, setCurrentTab] = useState(0);

  const tabs = [
    "Основные сведения",
    "Сведения об отлове",
    "Сведения о новых владельцах",
    "Движение животного",
    "Сведения об обработке от экто- и эндопаразитов",
    "Сведения о вакцинации",
    "Сведения о состоянии здоровья",
  ];

  const onTabChange = (event, newValue) => {
    setCurrentTab(newValue);
  };

  const renderTabs = () => {
    return (
      <Tabs
        className={classes.tabs}
        orientation={"vertical"}
        variant={"scrollable"}
        value={currentTab}
        onChange={onTabChange}
      >
        {tabs.map((tab, index) => (
          <Tab key={tab} label={tab} {...a11yProps(index)} />
        ))}
      </Tabs>
    );
  };

  return (
    <div>
      <Typography className={classes.title} variant={"h5"}>
        Карточка учёта животного
      </Typography>

      <Paper className={classes.root}>
        <Grid container spacing={0}>
          <Grid item xs={3}>
            {renderTabs()}
          </Grid>
          <Grid item xs={9}>
            <Formik
              initialValues={initialValues}
              onSubmit={(values, { setSubmitting }) => {
                setTimeout(() => {
                  setSubmitting(false);
                  alert(JSON.stringify(values, null, 2));
                }, 500);
              }}
            >
              <Form>
                <TabPanel value={currentTab} index={0}>
                  <BasicInfoForm />
                </TabPanel>
                <TabPanel value={currentTab} index={1}>
                  <CaptureForm />
                </TabPanel>
                <TabPanel value={currentTab} index={2}>
                  <NewOwnersForm />
                </TabPanel>
                <TabPanel value={currentTab} index={3}>
                  <PetMovement />
                </TabPanel>
                <TabPanel value={currentTab} index={4}>
                  <ParasitesTreatment />
                </TabPanel>
                <TabPanel value={currentTab} index={5}>
                  <Vaccination />
                </TabPanel>
                <TabPanel value={currentTab} index={6}>
                  <HealthStatus />
                </TabPanel>
              </Form>
            </Formik>
          </Grid>
        </Grid>

        <div className={classes.actions}>
          <Button
            variant={"contained"}
            color={"primary"}
            disableRipple
            className={classes.createButton}
          >
            Создать
          </Button>
        </div>
      </Paper>
    </div>
  );
};

export default CreatePetCard;
