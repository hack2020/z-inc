import React, { useState } from "react";
import { Fab, Typography } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import AddIcon from "@material-ui/icons/Add";

import { PET_CARD_FIELDS } from "../../assets/addCardFields";
import { renderField } from "./renderFormField";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles({
  addRecordBtn: {
    float: "right",
    margin: "10px 0px",
  },
});

const ParasitesTreatment = () => {
  const classes = useStyles();
  const [recordsCount, setRecordsCount] = useState(2);

  const renderRecords = () => {
    let records = [];
    for (let i = 0; i < recordsCount; i++) {
      PET_CARD_FIELDS.parasites_treatment.map((field) =>
        records.push(renderField(field, 4, field.name + i, field.name + i))
      );
    }
    return records;
  };

  const onAddClick = () => {
    setRecordsCount(recordsCount + 1);
  };

  return (
    <React.Fragment>
      <Typography variant={"h6"}>
        Сведения об обработке от экто- и эндопаразитов
      </Typography>
      <Grid container spacing={1}>
        {renderRecords()}
      </Grid>
      <Fab className={classes.addRecordBtn} size={"small"} onClick={onAddClick}>
        <AddIcon />
      </Fab>
    </React.Fragment>
  );
};

export default ParasitesTreatment;
