import React, { useState } from "react";
import { makeStyles } from "@material-ui/styles";
import Box from "@material-ui/core/Box";
import { Tabs, Typography } from "@material-ui/core";
import Tab from "@material-ui/core/Tab";
import Grid from "@material-ui/core/Grid";
import BasicInfoForm from "./BasicInfoForm";
import CaptureForm from "./CaptureForm";
import NewOwnersForm from "./NewOwnersForm";
import PetMovement from "./PetMovement";
import ParasitesTreatment from "./ParasitesTreatment";
import Vaccination from "./Vaccination";
import HealthStatus from "./HealthStatus";

const useStyle = makeStyles((theme) => ({
  root: {
    // flexGrow: 1,
    backgroundColor: "#fff",
    minHeight: "60rem",
  },
  tabs: {
    borderRight: `1px solid rgba(0, 0, 0, 0.12)`,
    width: "90%",
  }
}));

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

const PetCardTabs = () => {
  const classes = useStyle();
  const [currentTab, setCurrentTab] = useState(0);

  const tabs = [
    "Основные сведения",
    "Сведения об отлове",
    "Сведения о новых владельцах",
    "Движение животного",
    "Сведения об обработке от экто- и эндопаразитов",
    "Сведения о вакцинации",
    "Сведения о состоянии здоровья",
  ];

  const pages = [
    <BasicInfoForm />,
    <CaptureForm />,
    <NewOwnersForm />,
    <PetMovement />,
    <ParasitesTreatment />,
    <Vaccination />,
    <HealthStatus />,
  ];

  const onTabChange = (event, newValue) => {
    setCurrentTab(newValue);
  };

  const renderTabs = () => {
    return (
      <Tabs
        className={classes.tabs}
        orientation={"vertical"}
        variant={"scrollable"}
        value={currentTab}
        onChange={onTabChange}
      >
        {tabs.map((tab, index) => (
          <Tab key={tab} label={tab} {...a11yProps(index)} />
        ))}
      </Tabs>
    );
  };

  return (
    <React.Fragment>
        <Grid container>
            <Grid item>
                {renderTabs()}
            </Grid>
            <Grid item>
                {pages.map((page, i) =>
                  <TabPanel value={currentTab} index={0}>{page}</TabPanel>
                )}
            </Grid>
        </Grid>
    </React.Fragment>
  );
};

export default PetCardTabs;