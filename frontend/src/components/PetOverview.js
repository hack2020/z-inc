import React, { useState } from "react";
import { connect } from "react-redux";
import { Grid } from "@material-ui/core";

import PetDetail from "./PetDetail";
import ShelterCard from "./ShelterCard";

const PetOverview = (props) => {
  const { petId } = props.match.params;
  const [shelterId, setShelterId] = useState(null);

  const onPetLoad = (shelterId) => {
    setShelterId(shelterId);
  };

  return (
    <Grid container spacing={2}>
      <Grid item xs={8}>
        <PetDetail petId={petId} onLoad={onPetLoad} />
      </Grid>
      <Grid item xs={4}>
        <ShelterCard shelterId={shelterId} />
      </Grid>
    </Grid>
  );
};

const mapStateToProps = (state, ownProps) => {
  return {
    pet: state.pets[ownProps.match.params.petId],
  };
};

export default connect(mapStateToProps)(PetOverview);
