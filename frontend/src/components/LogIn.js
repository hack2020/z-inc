import React, { useState } from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Modal from "./Modal";
import Button from "@material-ui/core/Button";
import { Link } from "@material-ui/core";
import { Formik, Form, Field } from "formik";
import { TextField } from "formik-material-ui";
import * as Yup from "yup";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles({
  loginForm: {
    textAlign: "center",
    padding: "0px 0px 14px 0px",
  },
  loginBtn: {},
});

const LogIn = (props) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  const renderContent = () => {
    return (
      <Formik
        initialValues={{ email: "", password: "" }}
        validationSchema={Yup.object({
          email: Yup.string()
            .email("Некорректный email адрес")
            .required("Введите email адрес"),
          password: Yup.string().required("Введите пароль"),
        })}
        onSubmit={(values, { setSubmitting }) => {
          setTimeout(() => {
            setSubmitting(false);
            alert(JSON.stringify(values, null, 2));
          }, 500);
        }}
      >
        <Form id="login-form" className={classes.loginForm}>
          <Grid container spacing={2} justify={"center"}>
            <Grid item xs={10}>
              <Field
                variant="outlined"
                component={TextField}
                name="email"
                type="email"
                label="Email"
                fullWidth
              />
            </Grid>
            <Grid item xs={10}>
              <Field
                variant="outlined"
                component={TextField}
                name="password"
                type="password"
                label="Пароль"
                fullWidth
              />
            </Grid>
            <Grid item xs={10}>
              {renderActions()}
            </Grid>
          </Grid>
        </Form>
      </Formik>
    );
  };

  const renderActions = () => {
    return (
      <Button
        className={classes.loginBtn}
        variant={"outlined"}
        form="login-form"
        type="submit"
        disableRipple
        fullWidth
      >
        Войти
      </Button>
    );
  };

  return (
    <>
      <Link onClick={() => setOpen(true)}>Войти</Link>
      <Modal
        open={open}
        onClose={() => setOpen(false)}
        title={"Войти"}
        content={renderContent()}
      />
    </>
  );
};

export default LogIn;
