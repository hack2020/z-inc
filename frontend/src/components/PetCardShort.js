import React from "react";
import Card from "@material-ui/core/Card";
import {
  CardActionArea,
  CardContent,
  CardMedia,
  IconButton,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { Link } from "react-router-dom";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

const useStyles = makeStyles({
  root: {
    minWidth: 150,
    maxWidth: 350,
  },
  image: {
    height: 200,
  },
  cardContent: {
    padding: "16px 10px",
  },
  petsName: {
    fontSize: "1.3rem",
    display: "flex",
    justifyContent: "center",
  },
  petDescription: {
    display: "flex",
    justifyContent: "center",
  },
  textSeparator: {
    padding: "0px 5px",
  },
  actions: {
    display: "flex",
    justifyContent: "flex-end",
  },
});

const PetCardShort = ({ pet }) => {
  const classes = useStyles();
  const [expandedId, setExpandedId] = React.useState(false);

  const handleExpandClick = (i) => {
    setExpandedId(expandedId === i ? -1 : i);
  };

  const petDetailLink = `/${pet["вид"].toLowerCase()}/${
    pet["карточка_учета_животного"]
  }`;

  const getAge = () => {
    const currentYear = new Date().getFullYear();
    const age = currentYear - parseInt(pet["возраст"]);
    if (age < 2) {
      return pet["пол"].toLowerCase() === "мужской" ? "молодой" : "молодая";
    }
    return age + " " + "лет";
  };

  return (
    <Card className={classes.root}>
      <CardActionArea disableRipple component={Link} to={petDetailLink}>
        <CardMedia
          className={classes.image}
          title={pet["кличка"]}
          image={pet.photo}
        />
        <CardContent className={classes.cardContent}>
          <Typography className={classes.petsName} variant={"h5"}>
            {pet["кличка"]}
          </Typography>
          <Typography className={classes.petDescription} variant={"body2"}>
            {getAge()}
            <span className={classes.textSeparator}>&#8226;</span>
            {pet["порода"]}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions className={classes.actions}>
        <IconButton
          onClick={() => setExpandedId(!expandedId)}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expandedId} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>
            карточка животного: {pet["карточка_учета_животного"]}{" "}
          </Typography>
          <Typography paragraph>вид: {pet["вид"]} </Typography>
          <Typography paragraph>{pet["пол"]} </Typography>
          <Typography paragraph>
            идентификационная_метка: {pet["идентификационная_метка"]}
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
};

export default PetCardShort;
