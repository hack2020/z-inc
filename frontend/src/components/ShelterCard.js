import React, { useEffect } from "react";
import Card from "@material-ui/core/Card";
import CircularProgress from "@material-ui/core/CircularProgress";
import { Button, CardContent, CardHeader, Typography } from "@material-ui/core";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import MailIcon from "@material-ui/icons/Mail";
import PhoneIcon from "@material-ui/icons/Phone";
import { connect } from "react-redux";
import ListItemText from "@material-ui/core/ListItemText";
import Link from "@material-ui/core/Link";
import CardActions from "@material-ui/core/CardActions";

import { fetchShelter } from "../actions";

const ShelterCard = (props) => {
  const { shelterId } = props;
  const { shelter, fetchShelter } = props;

  useEffect(() => {
    if (shelterId) {
      fetchShelter(shelterId);
    }
  }, [fetchShelter, shelterId]);

  if (!shelter) {
    return (
      <div>
        <CircularProgress color={"primary"} />
      </div>
    );
  }

  return (
    <Card>
      <CardHeader title={shelter.name} />
      <CardContent>
        <List>
          <ListItem>
            <ListItemIcon>
              <LocationOnIcon />
            </ListItemIcon>
            <ListItemText
              primary={shelter.address}
              secondary={
                <Typography>
                  {shelter.city}, {shelter.district}
                </Typography>
              }
            />
          </ListItem>

          <ListItem>
            <ListItemIcon>
              <MailIcon />
            </ListItemIcon>
            <ListItemText
              primary={
                <Typography component={Link} href={`mailto:${shelter.email}`}>
                  {shelter.email}
                </Typography>
              }
            />
          </ListItem>

          {shelter.phone && (
            <ListItem>
              <ListItemIcon>
                <PhoneIcon />
              </ListItemIcon>
              <ListItemText
                primary={
                  <Typography component={Link} href={`tel:${shelter.phone}`}>
                    {shelter.phone}
                  </Typography>
                }
              />
            </ListItem>
          )}
        </List>
      </CardContent>
      <CardActions>
        <Button variant={"outlined"} disableRipple>
          Подробнее о "{shelter.name}"
        </Button>
      </CardActions>
    </Card>
  );
};

const mapStateToProps = (state, ownProps) => {
  return {
    shelter: state.shelters[ownProps.shelterId],
  };
};

export default connect(mapStateToProps, { fetchShelter })(ShelterCard);
