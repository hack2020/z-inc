import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles({
  closeBtn: {
    float: "right",
  },
  dialogTitle: {
    paddingTop: 0,
    textAlign: "center",
  },
});

const Modal = (props) => {
  const { open, title, content, actions, onClose } = props;
  const classes = useStyles();

  return (
    <Dialog open={open} onBackdropClick={onClose} maxWidth={"xs"} fullWidth>
      <div>
        <IconButton
          className={classes.closeBtn}
          onClick={onClose}
          disableRipple
        >
          <CloseIcon />
        </IconButton>
      </div>
      <DialogTitle className={classes.dialogTitle}>{title}</DialogTitle>
      <DialogContent>{content}</DialogContent>
      <DialogActions>{actions}</DialogActions>
    </Dialog>
  );
};

export default Modal;
