import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { DataGrid } from "@material-ui/data-grid";

import { fetchPets } from "../../actions";

const columns = [
  { field: "id", headerName: "#", width: 70, headerAlign: "center" },
  {
    field: "petCard",
    headerName: "Карточка животного",
    width: 180,
    headerAlign: "center",
  },
  { field: "petName", headerName: "Кличка", width: 120, headerAlign: "center" },
  { field: "petType", headerName: "Вид", width: 100, headerAlign: "center" },
  { field: "gender", headerName: "Пол", width: 120, headerAlign: "center" },
  {
    field: "identificationMark",
    headerName: "Идентификационная метка",
    width: 220,
    headerAlign: "center",
  },
  {
    field: "shelterArrivalDate",
    headerName: "Дата поступления в приют",
    width: 250,
    headerAlign: "center",
  },
];

const PetRegistry = (props) => {
  const { fetchPets, pets, petType } = props;
  const [records, setRecords] = useState([]);

  useEffect(() => {
    fetchPets(petType);
  }, [fetchPets, petType]);

  useEffect(() => {
    const rowsTemp = [];
    if (pets) {
      pets.map((pet, i) =>
        rowsTemp.push({
          id: i + 1,
          petCard: pet["карточка_учета_животного"],
          petName: pet["кличка"],
          petType: pet["вид"],
          gender: pet["пол"],
          identificationMark: pet["идентификационная_метка"],
          shelterArrivalDate: pet["возраст"],
        })
      );
      setRecords(rowsTemp);
    }
  }, [pets]);

  return (
    <div>
      <div style={{ height: 400, width: "100%" }}>
        <DataGrid rows={records} columns={columns} pageSize={20} autoHeight checkboxSelection />
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    pets: Object.values(state.pets),
  };
};

export default connect(mapStateToProps, { fetchPets })(PetRegistry);
