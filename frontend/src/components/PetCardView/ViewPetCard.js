import React, {useState} from "react";
import {makeStyles} from "@material-ui/styles";
import Box from "@material-ui/core/Box";
import {Tabs, Typography} from "@material-ui/core";
import Tab from "@material-ui/core/Tab";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

const useStyle = makeStyles((theme) => ({
    root: {
        // flexGrow: 1,
        backgroundColor: "#fff",
        minHeight: "60rem",
    },
    title: {
        textAlign: "center",
        padding: "12px 0px",
    },
    tabs: {
        borderRight: `1px solid rgba(0, 0, 0, 0.12)`,
        width: "90%",
    }
}));

function TabPanel(props) {
    const {children, value, index, ...other} = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`vertical-tabpanel-${index}`}
            aria-labelledby={`vertical-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

function a11yProps(index) {
    return {
        id: `vertical-tab-${index}`,
        "aria-controls": `vertical-tabpanel-${index}`,
    };
}

const ViewPetCard = ({pet}) => {
    const classes = useStyle();
    const [currentTab, setCurrentTab] = useState(0);

    const tabs = [
        "Основные сведения",
        "Сведения об отлове",
        "Сведения о новых владельцах",
        "Движение животного",
        "Сведения об обработке от экто- и эндопаразитов",
        "Сведения о вакцинации",
        "Сведения о состоянии здоровья",
    ];
    const onTabChange = (event, newValue) => {
        setCurrentTab(newValue);
    };

    const renderTabs = () => {
        return (
            <Tabs
                className={classes.tabs}
                orientation={"vertical"}
                variant={"scrollable"}
                value={currentTab}
                onChange={onTabChange}
            >
                {tabs.map((tab, index) => (
                    <Tab key={tab} label={tab} {...a11yProps(index)} />
                ))}
            </Tabs>
        );
    };

    return (
        <Paper>
            <Typography className={classes.title} variant={"h5"}>Карточка учёта животного</Typography>
            <Grid container spacing={0}>
                <Grid item xs={3}>
                    {renderTabs()}
                </Grid>
                <Grid item xs={9}>
                    <TabPanel value={currentTab} index={0}>
                    </TabPanel>
                    <TabPanel value={currentTab} index={1}>
                    </TabPanel>
                    <TabPanel value={currentTab} index={2}>
                    </TabPanel>
                    <TabPanel value={currentTab} index={3}>
                    </TabPanel>
                    <TabPanel value={currentTab} index={4}>
                    </TabPanel>
                    <TabPanel value={currentTab} index={5}>
                    </TabPanel>
                    <TabPanel value={currentTab} index={6}>
                    </TabPanel>
                </Grid>
            </Grid>
        </Paper>
    )
}

export default ViewPetCard;
