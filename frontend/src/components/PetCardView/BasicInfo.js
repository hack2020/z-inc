import React from "react";
import {Grid, Typography} from "@material-ui/core";

const BasicInfo = ({pet}) => {

    return (
        <Grid container spacing={2}>
            <Grid item>
                <Typography>г. Москва</Typography>
            </Grid>
            <Grid item>
                <Typography>"г.Москва, ул.Брусилова, вл.32, стр.1-5"</Typography>
            </Grid>
        </Grid>
    )

}

export default BasicInfo;