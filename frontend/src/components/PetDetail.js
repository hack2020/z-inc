import React, { useEffect } from "react";
import { connect } from "react-redux";
import { fetchPet } from "../actions";
import Paper from "@material-ui/core/Paper";
import { Divider, Typography } from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles } from "@material-ui/styles";

// todo: add favorite button
const useStyles = makeStyles({
  root: {
    // width: "80%",
    padding: 30,
  },
  divider: {
    margin: "8px 0px",
  },
  petName: {
    fontSize: "2rem",
    padding: "10px 0px 18px 0px",
  },
  petAboutHeader: {
    fontSize: "1.3rem",
    padding: "12px 0px 5px 0px",
    // fontWeight: "bold",
  },
  petStoryHeader: {
    fontSize: "1.5rem",
    padding: "12px 0px 5px 0px",
  },
  petStoryText: {
    textAlign: "justify",
  },
});

const PetDetail = (props) => {
  const classes = useStyles();
  const { petId } = props;
  const { pet, fetchPet, onLoad } = props;

  useEffect(() => {
    fetchPet(petId);
  }, [fetchPet, petId]);

  useEffect(() => {
    if (pet) {
      onLoad(pet.shelter_id);
    }
  }, [pet, onLoad]);

  if (!pet) {
    return (
      <div>
        <CircularProgress color={"primary"} />
      </div>
    );
  }

  const petPhoto = pet.media.find((image) => image.main === true);

  return (
    <Paper className={classes.root}>
      <img alt={pet.name} src={petPhoto.link} />
      <Typography className={classes.petName}>{pet.name}</Typography>
      <Typography>{pet.breed}. Москва, Кунцево</Typography>
      <Divider className={classes.divider} />
      <Typography>
        {pet.age} &#8226; {pet.gender} &#8226; среднего размера &#8226;
        гладкошёрстный
      </Typography>

      <Divider className={classes.divider} />

      <div className={classes.petAboutSection}>
        <Typography className={classes.petAboutHeader}>Шерсть</Typography>
        <Typography>короткая</Typography>
        <Typography className={classes.petAboutHeader}>Здоровье</Typography>
        <Typography>Прививки актуальные. Обработан от паразитов</Typography>
      </div>

      <Divider className={classes.divider} />

      <div className={classes.petStorySection}>
        <Typography className={classes.petStoryHeader}>
          Подробнее о {pet.name}
        </Typography>
        <Typography className={classes.petStoryText} variant={"body1"}>
          {pet.description}
        </Typography>
      </div>
      {JSON.stringify(pet)}
    </Paper>
  );
};

const mapStateToProps = (state, ownProps) => {
  return {
    pet: state.pets[ownProps.petId],
  };
};

export default connect(mapStateToProps, { fetchPet })(PetDetail);
