import React from "react";
import {
  AppBar,
  IconButton,
  makeStyles,
  Toolbar,
  Typography,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import FavoriteIcon from "@material-ui/icons/Favorite";
import clsx from "clsx";
import PetsIcon from "@material-ui/icons/Pets";
import Container from "@material-ui/core/Container";
import { connect } from "react-redux";

import LogIn from "./LogIn";

const headerStyle = makeStyles((theme) => ({
  root: {
    padding: "0px 0px 16px 0px",
  },
  toolbar: {
    justifyContent: "space-between",
    padding: 0,
  },
  logo: {
    padding: "8px 10px 8px 0px",
    color: "purple",
  },
  title: {
    display: "flex",
    alignItems: "center",
    color: "black",
  },
  homeLink: {
    textDecoration: "none",
  },
  link: {
    textDecoration: "none",
  },
  appBarIcon: {
    color: "purple",
  },
  rightMenu: {},
}));

// todo: do i need a search component?
const Header = (props) => {
  const classes = headerStyle();
  return (
    <div className={classes.root}>
      <AppBar color="transparent" variant="outlined" position={"relative"}>
        <Container>
          <Toolbar className={classes.toolbar}>
            <div className={classes.title}>
              <Link to={"/"}>
                <PetsIcon className={classes.logo} />
              </Link>
              <Typography
                variant="h6"
                className={clsx(classes.title, classes.link)}
                component={Link}
                to={"/"}
              >
                Наташа, мы уронили вообще всё
              </Typography>
            </div>

            <div>
              <Typography
                className={classes.link}
                component={Link}
                to={"/pets/list"}
              >
                Список животных
              </Typography>
            </div>

            <div className={classes.rightMenu}>
              <IconButton>
                <FavoriteIcon className={classes.appBarIcon} />
              </IconButton>
              <LogIn />
            </div>
          </Toolbar>
        </Container>
      </AppBar>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};

export default connect(mapStateToProps)(Header);
