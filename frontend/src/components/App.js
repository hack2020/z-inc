import React from "react";
import { Container } from "@material-ui/core";
import { BrowserRouter, Route } from "react-router-dom";

import Header from "./Header";
import HomePage from "./HomePage";
import CreatePetCard from "./PetCardCreate/CreatePetCard";
import ViewPetCard from "./PetCardView/ViewPetCard";
import PetRegistry from "./PetRegistry/PetRegistry";



function App() {
  return (
    <div className="App">
        <BrowserRouter>
          <div>
            <Header />
            <Container>
              <Route path="/" exact component={HomePage} />
              <Route path="/pets/list" exact component={PetRegistry} />
              <Route path="/create_pet_card" exact component={CreatePetCard} />
              <Route path="/:shelterId/pets_registry" exact component={CreatePetCard} />
              <Route path="/detail/:petType/:petId" component={ViewPetCard} />
            </Container>
          </div>
        </BrowserRouter>
    </div>
  );
}

export default App;
