import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Grid } from "@material-ui/core";
import { fetchPets } from "../actions";
import PetCardShort from "./PetCardShort";

const PetCardList = (props) => {
  const { fetchPets, pets, petType } = props;

  useEffect(() => {
    fetchPets(petType);
  }, [fetchPets, petType]);

  return (
    <div>
      <Grid container spacing={3}>
        {pets.map((pet) => (
          <Grid key={pet["кличка"]} item xs={6} sm={4} md={4} lg={3}>
            <PetCardShort pet={pet} />
          </Grid>
        ))}
      </Grid>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    pets: Object.values(state.pets),
  };
};

export default connect(mapStateToProps, { fetchPets })(PetCardList);
