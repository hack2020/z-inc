export const PET_CARD_FIELDS = {
    general: [
        // city: {name: "Город", type: "text"},
        // date: {name: "", type: "date"},
        // shelter_address: {name: "Адрес приюта", type: "text"},
        // operating_organization: {name: "Эксплуатирующая организация", type: "text"},
        // aviary: {name: "Номер вольера", type: "text"}
        {name: "city", verbose_name: "Город", type: "text"},
        {name: "date", verbose_name: "Дата", type: "date"},
        {name: "shelter_address", verbose_name: "Адрес приюта", type: "text"},
        {name: "operating_organization", verbose_name: "Эксплуатирующая организация", type: "text"},
        {name: "aviary", verbose_name: "Номер вольера", type: "text"}
    ],
    main: [
        // petType: {verbose_name: "", type: "select_radio", options: ["собака", "кошка"]},
        // age: {verbose_name: "Возраст", type: "number"},
        // weight: {verbose_name: "Вес", type: "number"},
        // petName: {verbose_name: "Кличка", type: "text"},
        // gender: {verbose_name: "Пол", type: "select_radio", options: ["женский", "мужской"]},
        // breed: {verbose_name: "Порода", type: "text"},
        // color: {verbose_name: "Окрас", type: "text"},
        // coat: {verbose_name: "Шерсть", type: "text"},
        // ears: {verbose_name: "Уши", type: "text"},
        // tail: {verbose_name: "Хвост", type: "text"},
        // size: {verbose_name: "Размер", type: "text"}
        {name: "petType", verbose_name: "", type: "select_radio", options: ["собака", "кошка"]},
        {name: "age", verbose_name: "Возраст", type: "number"},
        {name: "weight", verbose_name: "Вес", type: "number"},
        {name: "petName", verbose_name: "Кличка", type: "text"},
        {name: "gender", verbose_name: "Пол", type: "select_radio", options: ["женский", "мужской"]},
        {name: "breed", verbose_name: "Порода", type: "text"},
        {name: "color", verbose_name: "Окрас", type: "text"},
        {name: "coat", verbose_name: "Шерсть", type: "text"},
        {name: "ears", verbose_name: "Уши", type: "text"},
        {name: "tail", verbose_name: "Хвост", type: "text"},
        {name: "size", verbose_name: "Размер", type: "text"},
        {name: "specialSigns", verbose_name: "Особые приметы", type: "text"},
        {name: "petCharacter", verbose_name: "Характер", type: "text"},
        {name: "identificationMark", verbose_name: "Идентификационная метка", type: "text"},
        {name: "sterilizationDate", verbose_name: "Дата стерилизации", type: "date"},
        {name: "sterilizationSite", verbose_name: "Место стерилизации", type: "text"},
        {name: "veterinarianName", verbose_name: "ФИО ветеринарного врача", type: "text"},
        {name: "socialized", verbose_name: "Социализировано/готово к пристройству", type: "check"}
    ],
    capture: [
        {name: "orderPetReceiptNum", verbose_name: "Заказ-наряд №", type: "text"},
        {name: "orderPetReceiptDate", verbose_name: "Дата", type: "date"},
        {name: "captureActNum", verbose_name: "Акт отлова №", type: "text"},
        {name: "captureActDate", verbose_name: "Дата", type: "date"},
        {name: "captureAddress", verbose_name: "Адрес места отлова ", type: "text"},
        {name: "videoRecordOfCapture", verbose_name: "Видеофиксация отлова", type: "text"},
    ],
    new_owners_info: {
        legalEntity: [{name: "legalEntityName", verbose_name: "Юридическое лицо", type: "text"},
            {name: "legalEntityAddress", verbose_name: "Адрес", type: "text"},
            {name: "legalEntityPhone", verbose_name: "Телефон", type: "text"},
            {name: "guardiansName", verbose_name: "ФИО опекунов", type: "text"},
            {name: "guardiansContacts", verbose_name: "Контактные данные", type: "text"}],
        individual: [
            {name: "individualName", verbose_name: "Физическое лицо (ФИО)", type: "text"},
            {name: "passport", verbose_name: "Паспорт РФ", type: "text"},
            {name: "passportIssued", verbose_name: "Выдан", type: "date"},
            {name: "passportRegisterAddress", verbose_name: "Зарегестрированный(ая) по адресу", type: "text"},
            {name: "individualContacts", verbose_name: "Контактные данные", type: "text"},
        ]
    },
    pet_movements: [
        {name: "shelterArrivalDate", verbose_name: "Дата поступления в приют", type: "date"},
        {name: "shelterArrivalActNum", verbose_name: "Акт №", type: "text"},
        {name: "shelterLeaveDate", verbose_name: "Дата выбытия из приюта", type: "date"},
        {name: "shelterLeaveActNum", verbose_name: "Акт №", type: "date"},
        {name: "shelterLeaveReason", verbose_name: "Причина выбытия из приюта", type: "text"}
    ],
    parasites_treatment: [
        {name: "parasitesTreatmentDate", verbose_name: "Дата", type: "date"},
        {name: "treatmentDrug", verbose_name: "Препарат", type: "text"},
        {name: "treatmentDose", verbose_name: "Доза", type: "text"},
    ],
    vaccination: [
        {name: "vaccinationDate", verbose_name: "Дата", type: "date"},
        {name: "vaccineName", verbose_name: "Вид вакцины", type: "text"},
        {name: "vaccineSeries", verbose_name: "№ серии", type: "text"},
    ],
    health_status: [
        {name: "inspectionDate", verbose_name: "Дата осмотра", type: "date"},
        {name: "weight", verbose_name: "Вес", type: "number"},
        {name: "anamnesis", verbose_name: "Анамнез", type: "text"}
    ]
}

