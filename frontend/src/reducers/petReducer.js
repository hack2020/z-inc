import _ from "lodash";
import { FETCH_PETS, FETCH_PET } from "../actions/types";

export default (state = {}, action) => {
  console.log(action);
  switch (action.type) {
    case FETCH_PETS:
      return action.payload.slice(0, 50);
    case FETCH_PET:
      return { ...state, [action.payload.id]: action.payload };
    default:
      return state;
  }
};
