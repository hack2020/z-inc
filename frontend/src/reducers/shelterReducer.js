import { FETCH_SHELTER } from "../actions/types";

export default (state = {}, action) => {
  switch (action.type) {
    case FETCH_SHELTER:
      return { ...state, [action.payload.id]: action.payload };
    default:
      return state;
  }
};
