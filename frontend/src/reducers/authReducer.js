const initialState = {
  isSignedIn: false,
  userId: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
