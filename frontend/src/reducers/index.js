import { combineReducers } from "redux";
import authReducer from "./authReducer";
import petReducer from "./petReducer";
import shelterReducer from "./shelterReducer";

export default combineReducers({
  user: authReducer,
  pets: petReducer,
  shelters: shelterReducer,
});
