#!/bin/bash
### dependencies:
#sudo apt install rename
cd pets/uploads/

find . -type f -name '*.jpeg' -print0 | xargs -0 rename 's/\.jpeg/\.jpg/'
find . -type f -name '*.JPG' -print0 | xargs -0 rename 's/\.JPG/\.jpg/'
mogrify -format jpg *.png
rm *.png

echo 'done'
