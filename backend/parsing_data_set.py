### dependences:
# pip3 install pandas
# pip3 install sqlalchemy


import pandas as pd
from sqlalchemy import create_engine
from datetime import datetime
 
file = ("data/full_data_set.xlsx")

df = pd.read_excel(file)
df['created'] = datetime.now()
df.head()

for index in df.index:
  df.loc[index, 'вид'] = df.loc[index, 'вид'].title()
  df.loc[index, 'пол'] = df.loc[index, 'пол'].title()
  df.loc[index, 'cоциализировано'] = df.loc[index, 'cоциализировано'].title()
  df.loc[index, 'photo'] = 'uploads/' + str(df.loc[index, 'карточка_учета_животного']) + '.jpg'
#  print(df.loc[index, 'photo'])
    
### Example connect to another database 
#database_connect = create_engine('mysql://USER:PASS@HOST/DB_NAME')
database_connect = create_engine('sqlite:///db.sqlite3')
df.to_sql('pets_pet', con=database_connect, if_exists='append', index=False)

#shelters_data = df.filter(items=['акт', 'вид'])
#print(shelters_data)

#df.to_sql('pets_shelters', con=database_connect, if_exists='append', index=False)
