from django.conf import settings
from django.db import models
# TYPE_CHOICES = (
#     ('dog', 'Собака'),
#     ('cat', 'Кошка'),
# )

class Pet(models.Model):

    карточка_учета_животного = models.CharField(max_length=1000 ,verbose_name='карточка учета животного №', blank=True)
    вид = models.CharField(max_length=1000, verbose_name='вид', blank=True)
    возраст = models.TextField(max_length=1000, verbose_name='возраст, год', blank=True)
    вес = models.FloatField(max_length=1000, verbose_name='вес, кг', blank=True)
    кличка = models.CharField(max_length=1000, verbose_name='кличка', blank=True, null=True)
    пол = models.CharField(max_length=1000, verbose_name='пол', blank=True)
    порода = models.CharField(max_length=1000, verbose_name='порода', blank=True, null=True)
    окрас = models.CharField(max_length=1000, verbose_name='окрас', blank=True, null=True)
    шерсть = models.CharField(max_length=1000, verbose_name='шерсть', blank=True, null=True)
    уши = models.CharField(max_length=1000, verbose_name='уши', blank=True)
    хвост = models.CharField(max_length=1000, verbose_name='хвост', blank=True)
    размер = models.CharField(max_length=1000, verbose_name='размер', blank=True, null=True)
    особые_приметы = models.CharField(max_length=1000, verbose_name='особые приметы', blank=True, null=True)
    Вольер = models.IntegerField(verbose_name='вольер №', blank=True, null=True)
    идентификационная_метка = models.CharField(max_length=1000, verbose_name='идентификационная метка', blank=True, null=True)
    дата_стерилизации = models.TextField(verbose_name='дата стерилизации', blank=True, null=True)
    ф_и_о_ветеринарного_врача = models.CharField(max_length=1000, verbose_name='ф.и.о.ветеринарного врача', blank=True, null=True)
    cоциализировано = models.BooleanField(verbose_name='социализировано', blank=True, null=True)
    заказ_наряд_акт_о_поступлении_животного = models.CharField(max_length=1000, verbose_name='заказ - наряд / акт о поступлении животного №', blank=True, null=True)
    заказ_наряд_дата_акт_о_поступлении_животного_дата = models.TextField(verbose_name='заказ - наряд дата / акт о поступлении животного, дата', blank=True, null=True)
    административный_округ = models.CharField(max_length=1000, verbose_name='административный округ', blank=True, null=True)
    акт_отлова = models.CharField(max_length=1000, verbose_name='акт отлова №', blank=True, null=True)
    адрес_места_отлова = models.CharField(max_length=1000, verbose_name='адрес места отлова', blank=True, null=True)
    юридическое_лицо = models.CharField(max_length=1000, verbose_name='юридическое лицо', blank=True, null=True)
    ф_и_о_опекунов = models.CharField(max_length=1000, verbose_name='ф.и.о. опекунов', blank=True, null=True)
    физическое_лицо_ф_и_о = models.CharField(max_length=1000, verbose_name='физическое лицо ф.и.о.', blank=True, null=True)
    дата_поступления_в_приют = models.TextField(verbose_name='дата поступления в приют', blank=True, null=True)
    акт = models.CharField(max_length=1000, verbose_name='акт №', blank=True, null=True)
    дата_выбытия_из_приюта = models.TextField(verbose_name='дата выбытия из приюта', blank=True, null=True)
    причина_выбытия = models.CharField(max_length=1000, verbose_name='причина выбытия', blank=True, null=True)
    акт_договор = models.CharField(max_length=1000, verbose_name='акт / договор №', blank=True, null=True)
    адрес_приюта = models.CharField(max_length=1000, verbose_name='адрес приюта', blank=True, null=True)
    эксплуатирующая_организация = models.CharField(max_length=1000, verbose_name='эксплуатирующая организация', blank=True, null=True)
    ф_и_о_руководителя_приюта = models.CharField(max_length=1000, verbose_name='ф.и.о.руководителя приюта', blank=True, null=True)
    ф_и_о_сотрудника_по_уходу_за_животным = models.CharField(max_length=1000, verbose_name='ф.и.о.сотрудника по  уходу за животным', blank=True, null=True)
    п_п_1 = models.CharField(max_length=1000, verbose_name='№ п / п', blank=True, null=True)
    дата_п_п_1 = models.TextField(verbose_name='дата', blank=True, null=True)
    название_препарата = models.CharField(max_length=1000, verbose_name='название препарата', blank=True, null=True)
    доза = models.CharField(max_length=1000, verbose_name='доза', blank=True, null=True)
    п_п_2 = models.CharField(max_length=1000, verbose_name='№ п / п', blank=True, null=True)
    дата_п_п_2 = models.TextField(verbose_name='дата', blank=True, null=True)
    вид_вакцины = models.CharField(max_length=1000, verbose_name='вид вакцины', blank=True, null=True)
    номер_серии = models.CharField(max_length=1000, verbose_name='№ серии', blank=True, null=True)
    дата_осмотра = models.TextField(verbose_name='дата осмотра', blank=True, null=True)
    анамнез = models.CharField(max_length=1000, verbose_name='анамнез', blank=True, null=True)
    photo = models.FileField(upload_to="uploads", blank=True)
    created = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return '{}'.format(self.карточка_учета_животного)

    class Meta:
        ordering = ['created']

class Shelter(models.Model):
    title = models.CharField(max_length=200, verbose_name="Название")
    short_description = models.TextField(max_length=500, verbose_name="Описание")
    address = models.CharField(max_length=1000)
    pets = models.ManyToManyField(Pet)
    created = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    def __str__(self):
        return '{}'.format(self.title)
