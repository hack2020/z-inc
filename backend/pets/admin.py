from django.contrib import admin
from import_export import resources
from .models import Pet, Shelter
from import_export.admin import ImportExportModelAdmin
from import_export import resources

class PetResource(resources.ModelResource):
    class Meta:
        model = Pet

class PetAdmin(ImportExportModelAdmin):
    resource_class = PetResource

class ShelterResource(resources.ModelResource):
    class Meta:
        model = Pet

class ShelterAdmin(ImportExportModelAdmin):
    resource_class = ShelterResource

admin.site.register(Pet, PetAdmin)
admin.site.register(Shelter, ShelterAdmin)

