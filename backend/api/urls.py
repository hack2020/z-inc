from django.conf.urls import re_path, include
from django.urls import path
from rest_framework.routers import DefaultRouter
from rest_framework.schemas import get_schema_view
from rest_framework.authtoken import views as auth_vews

from api import views

schema_view = get_schema_view(title='Pets API')

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'shelter', views.ShelterViewSet)
router.register(r'pet', views.PetViewSet)
from rest_framework.schemas import get_schema_view


# The API URLs are now determined automatically by the router.
urlpatterns = [
    re_path(r'^', include(router.urls)),
    path('schema/', schema_view),
    # path('auth/', auth_vews.obtain_auth_token),

]
