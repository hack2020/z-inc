from django.contrib.auth.models import User
from django_filters import filters
from rest_framework import serializers
from pets.models import Shelter, Pet

from django.contrib.auth import get_user_model

User = get_user_model()

class Petserializer(serializers.ModelSerializer):
    class Meta:
        model = Pet
        fields = ['карточка_учета_животного',
                  'photo',
                  'вид',
                  'возраст',
                  'вес',
                  'кличка',
                  'пол',
                  'порода',
                  'окрас',
                  'шерсть',
                  'уши',
                  'хвост',
                  'размер',
                  'особые_приметы',
                  'Вольер',
                  'идентификационная_метка',
                  'created'
                  ]


class Shelterserializer(serializers.ModelSerializer):
    pets = Petserializer(many=True)
    class Meta:
        model = Shelter
        fields = ['title', 'short_description', 'address', 'pets']


# class SelectAdditiveSerializer(serializers.ModelSerializer):
#     select_items = Petserializer(many=True)
#
#     class Meta:
#         model = SelectItem
#         fields = ['title',
#                   'select_items']
#
#
# class AdditiveSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Additive
#         fields = ['title',
#                   'price',
#                   'portion',
#                   'balance',
#                   'id']
#
#
# ### BudgetParticipant MODEL ###
# class MenuSerializer(serializers.ModelSerializer):
#     # owner = serializers.ReadOnlyField(source='owner.username')
#     additive = AdditiveSerializer(many=True)
#     select_additive = SelectAdditiveSerializer(many=True)
#
#     class Meta:
#         model = Menu
#         fields = (
#             'title',
#             'short_description',
#             'consist',
#             'photo',
#             'additive',
#             'select_additive',
#             'price',
#             'portion',
#             'balance',
#             'stop_list',
#             'id')
#
#
# class Orderserializer(serializers.ModelSerializer):
#     class Meta:
#         model = Order
#         fields = ['title',
#                   'order',
#                   'created',
#                   'user_id',
#                   'status',
#                   'total_price',
#                   'table',
#                   'payment_method',
#                   'id',]
