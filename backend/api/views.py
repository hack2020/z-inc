from django.shortcuts import render
from django.contrib.auth.models import User
from django.utils.datetime_safe import date
from django.views.decorators.csrf import csrf_exempt
from rest_framework import permissions, viewsets, status
from rest_framework.response import Response
from django_filters import rest_framework as filters
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from .permissions import IsOwnerOrReadOnly
from .serializers import Shelterserializer, Petserializer
# from .serializers import Orderserializer
# from .serializers import Menu
from pets.models import Shelter, Pet
# from order.models import Order
from django.contrib.auth import get_user_model
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from datetime import datetime

User = get_user_model()
# Create your views here.

class ShelterViewSet(viewsets.ModelViewSet):
    queryset = Shelter.objects.all()
    serializer_class = Shelterserializer

    # filter_backends = (filters.DjangoFilterBackend,)
    # filter_fields = ('id', 'stop_list')

    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
        IsOwnerOrReadOnly,)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

class PetViewSet(viewsets.ModelViewSet):
    queryset = Pet.objects.all()
    serializer_class = Petserializer

    # filter_backends = (filters.DjangoFilterBackend,)
    # filter_fields = ('id', 'stop_list')

    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
        IsOwnerOrReadOnly,)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
